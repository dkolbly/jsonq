package jsonq

import (
	"encoding/json"
	"testing"
)

var pathTestDoc = `
{
      "first": "John",
      "last": "Doe",
      "address": {
        "city": "austin",
        "state": "tx"
      },
      "phones": [
        {
          "type": "home",
          "number": "+1 123-456-7889"
        },
        {
          "type": "cell",
          "number": "+1 456-789-1234"
        },
        {
          "type": "work",
          "number": "+1 512-555-1234"
        }
      ],
      "data": {
        "content": [
          {
            "type": "normal",
            "content": "red"
          },
          {
            "type": "recur",
            "content": [
              {
		"type": "normal",
		"content": "red"
              },
              {
		"type": "normal",
		"content": "green",
                "flag": 2
              },
              {
		"type": "normal",
		"content": "blue",
                "flag": 1
              }
            ],
            "flag": 1
          },
          {
            "type": "normal",
            "content": "blue"
          }
        ]
    }
}`

type pathCase struct {
	given  string
	result []string
}

var pathCases = []pathCase{
	{ //0
		given:  `$`,
		result: []string{""},
	},
	{ //1
		given:  `$.foo`,
		result: []string{},
	},
	{ //2
		given:  `$.first`,
		result: []string{"first"},
	},
	{ //3
		given:  `$.address.city`,
		result: []string{"address.city"},
	},
	{ //4
		given: `$.phones..type`,
		result: []string{
			"phones.0.type",
			"phones.1.type",
			"phones.2.type",
		},
	},
	{ //5
		given: `$.data..[?(@.flag)]`,
		result: []string{
			"data.content.1",
			"data.content.1.content.1",
			"data.content.1.content.2",
		},
	},
	{ //6
		given: `$.phones[-2:]`,
		result: []string{
			"phones.1",
			"phones.2",
		},
	},
	{ //6
		given: `$.phones[-100000:100000:3]`,
		result: []string{
			"phones.2",
		},
	},
	{ //7
		given: `$..[?(@.type == "cell")]`,
		result: []string{
			"phones.1",
		},
	},
	{ //8
		given: `$.data..[?(@.flag >= 1)]`,
		result: []string{
			"data.content.1",
			"data.content.1.content.1",
			"data.content.1.content.2",
		},
	},
	{ //8
		given: `$.data..[?(@.flag >= 2)]`,
		result: []string{
			"data.content.1.content.1",
		},
	},
}

func TestPathEval(t *testing.T) {
	var doc interface{}
	err := json.Unmarshal([]byte(pathTestDoc), &doc)

	if err != nil {
		t.Fatal("Could not parse test doc")
	}

	for i, c := range pathCases {
		expr, err := PathParse(c.given)
		if err != nil {
			t.Errorf("Case[%d] expected to parse, but failed with: %s", i, err)
			continue
		}
		p := expr.Paths(doc)
		if len(p) != len(c.result) {
			actual := ""
			for j, a := range p {
				if j > 0 {
					actual = actual + ","
				}
				actual = actual + a.short()
			}
			t.Errorf("Case[%d] expected %d results, but produced %d:\n%s",
				i, len(c.result), len(p), actual)
		} else {
			for j, path := range p {
				if c.result[j] != path.short() {
					t.Errorf("Case[%d] result[%d] expected %s but got %s",
						i, j,
						c.result[j],
						path.short())
				}
			}
		}
	}

}
