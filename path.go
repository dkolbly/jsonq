package jsonq

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/dkolbly/logging"
	"github.com/dkolbly/logging/pretty"
)

func init() {
	pretty.Debug()
}

var log = logging.New("jsonq")

type Path struct {
	Steps []PropStep
	Value interface{}
}

var emptyPath = Path{}

type PropStep interface { // union of string and non-negative int
}

func (p Path) short() string {
	var lst []string
	for _, s := range p.Steps {
		if prop, ok := s.(string); ok {
			lst = append(lst, prop)
		} else {
			lst = append(lst, fmt.Sprintf("%d", s))
		}
	}
	return strings.Join(lst, ".")
}

type searchctx struct {
	result []Path
}

type pnode struct {
	step   PropStep
	parent *pnode
	len    int
}

func (pn *pnode) printlabel(buf *bytes.Buffer) {
	if pn.parent != nil {
		pn.parent.printlabel(buf)
		buf.WriteByte('.')
	}
	if prop, ok := pn.step.(string); ok {
		buf.WriteString(prop)
	} else {
		fmt.Fprintf(buf, "%d", pn.step)
	}
}

func (pn *pnode) short() string {
	if pn == nil {
		return ""
	}

	buf := &bytes.Buffer{}
	pn.printlabel(buf)
	return buf.String()
}

func (pn *pnode) cons(s PropStep) *pnode {
	n := 0
	if pn != nil {
		n = pn.len
	}
	return &pnode{
		step:   s,
		parent: pn,
		len:    n + 1,
	}
}

func (pn *pnode) mkpath(v interface{}) Path {
	if pn == nil {
		return emptyPath
	}
	steps := make([]PropStep, pn.len)
	n := pn
	for i := pn.len; i > 0; i-- {
		steps[i-1] = n.step
		n = n.parent
	}
	return Path{steps, v}
}

func (e *Expr) Paths(doc interface{}) []Path {
	ctx := &searchctx{}

	if !e.Anchored {
		panic("unanchored search not valid")
	}
	e.Filter.find(ctx, doc, nil)
	return ctx.result
}

func (f finalMatch) find(ctx *searchctx, doc interface{}, at *pnode) {
	ctx.result = append(ctx.result, at.mkpath(doc))
}

func (pf *propertyFilter) find(ctx *searchctx, doc interface{}, at *pnode) {
	dict, ok := doc.(map[string]interface{})
	if !ok {
		return
	}
	value, ok := dict[pf.propertyName]
	if !ok {
		return
	}
	pf.rest.find(ctx, value, at.cons(pf.propertyName))
}

func (sf *sliceFilter) find(ctx *searchctx, doc interface{}, at *pnode) {
	if array, ok := doc.([]interface{}); ok {
		n := int64(len(array))
		start := sf.spec.start
		if start < 0 {
			start = n + start
		}

		if sf.spec.flags&singleFlag != 0 {
			if start >= 0 && start < n {
				sf.rest.find(ctx, array[start], at.cons(start))
			}
			return
		}

		end := sf.spec.end
		if sf.spec.flags&endFlag == 0 {
			end = n
		} else if end < 0 {
			end = n + end
		}

		step := sf.spec.step
		if sf.spec.flags&stepFlag == 0 {
			step = 1
		}
		if start < 0 {
			start += step * (1 + (-start / step))
		}
		if end > n {
			end = n
		}
		log.Info("Scan start at [%d] end at [%d] step by +%d", start, end, step)
		for i := start; i < end; i += step {
			sf.rest.find(ctx, array[i], at.cons(i))
		}
	}
}

func indented(doc interface{}) string {
	buf, _ := json.MarshalIndent(doc, "", "  ")
	return string(buf)
}

func (ps *recursiveScanFilter) find(ctx *searchctx, doc interface{}, at *pnode) {
	ps.rest.find(ctx, doc, at)

	log.Info("recursive scan at %s for %s:\n%s", at.short(), ps.String(), indented(doc))

	if dict, ok := doc.(map[string]interface{}); ok {
		for k, v := range dict {
			ps.find(ctx, v, at.cons(k))
		}
		return
	} else if array, ok := doc.([]interface{}); ok {
		for i, v := range array {
			ps.find(ctx, v, at.cons(i))
		}
		return
	}
}

func (w *wildcardFilter) find(ctx *searchctx, doc interface{}, at *pnode) {
	panic("todo")
}

// [?(expr)]
//
// This is the one where we implicitly go over the attributes/elements
// of the document.
func (qs *qualifiedScan) find(ctx *searchctx, doc interface{}, at *pnode) {
	if dict, ok := doc.(map[string]interface{}); ok {
		for k, v := range dict {
			ret, err := qs.expr.eval(&jsonContext{v})
			if err != nil {
				log.Debug("qs(%s) %q error: %s", at.short(), k, err)
			} else {
				log.Debug("qs(%s) %q => %#v", at.short(), k, ret)
			}

			if err == nil && truish(ret) {
				qs.rest.find(ctx, v, at.cons(k))
			}
		}
	} else if array, ok := doc.([]interface{}); ok {
		for i, v := range array {
			ret, err := qs.expr.eval(&jsonContext{v})
			if err != nil {
				log.Debug("qs(%s) %d error: %s", at.short(), i, err)
			} else {
				log.Debug("qs(%s) %d => %#v", at.short(), i, ret)
			}
			if err == nil && truish(ret) {
				qs.rest.find(ctx, v, at.cons(i))
			}
		}
	}
}

// ?(expr)
// short circuit, somewhat like Prolog's cut operator?
// ...continue on if the expression is true, otherwise stop
func (post *postFilter) find(ctx *searchctx, doc interface{}, at *pnode) {
	ret, err := post.expr.eval(&jsonContext{doc})
	if err == nil && truish(ret) {
		post.rest.find(ctx, doc, at)
	}
}
