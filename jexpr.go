package jsonq

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"math"

	"bitbucket.org/dkolbly/earley"
)

var ErrMethodNotSupported = errors.New("method not supported")
var ErrNoSuchProperty = errors.New("no such property of object")
var ErrNotAnObject = errors.New("not an object")
var ErrNotSubscriptable = errors.New("not subscriptable (array or object)")
var ErrArrayIndexOutOfRange = errors.New("array index out of range")
var ErrNotStringable = errors.New("not a string")
var ErrNotNumeric = errors.New("not numeric")
var ErrIncommensurate = errors.New("incommensurate values")

type jsonContext struct {
	doc interface{}
}

func stringish(x interface{}) (string, error) {
	switch x := x.(type) {
	case string:
		return x, nil
	case json.Number:
		return x.String(), nil
	case int64:
		return fmt.Sprintf("%d", x), nil
	case float64:
		return fmt.Sprintf("%g", x), nil
	default:
		// TODO should we implement more weird JSON dynamic
		// stringification here?
		return "", ErrNotStringable
	}
}

func floatish(x interface{}) float64 {
	switch x := x.(type) {

	case json.Number:
		n, err := x.Float64()
		if err != nil {
			return math.NaN()
		}
		return n

	case int64:
		return float64(x)

	case float64:
		return x

	default:
		return math.NaN()
	}
}

func intish(x interface{}) (int64, error) {
	switch x := x.(type) {
	case json.Number:
		return x.Int64()
	case int64:
		return x, nil
	case float64:
		return int64(x), nil
	default:
		return 0, ErrNotNumeric
	}
}

type typecode uint8

const (
	jstring = typecode(iota)
	jnumber
	jobject
	jarray
	jnull
	jbool
	jundefined
)

func jtype(x interface{}) typecode {
	switch x.(type) {

	case bool:
		return jbool

	case string:
		return jstring

	case []interface{}:
		return jarray

	case map[string]interface{}:
		return jobject

	case json.Number, float64, int64:
		return jnumber

	case nil:
		return jnull

	default:
		return jundefined
	}
}

func compare(x, y interface{}) (bool, bool, bool, error) {
	xtype := jtype(x)
	ytype := jtype(y)

	if xtype == jnumber && ytype == jnumber {
		x := floatish(x)
		y := floatish(y)
		return x < y, x == y, x > y, nil
	} else if xtype == jstring || ytype == jstring {
		x, err := stringish(x)
		if err != nil {
			return false, false, false, err
		}
		y, err := stringish(y)
		if err != nil {
			return false, false, false, err
		}
		return x < y, x == y, x > y, nil
	} else if xtype == jbool && ytype == jbool {
		x := x.(bool)
		y := y.(bool)
		return false, x == y, false, nil
	} else {
		return false, false, false, ErrIncommensurate
	}
}

func truish(x interface{}) bool {
	switch x := x.(type) {

	case bool:
		return x

	case string:
		// strings are true if they are non-empty
		return len(x) > 0

	case []interface{}:
		// arrays are objects, so they're always true
		return true

	case map[string]interface{}:
		// objects are always true, even if they're empty
		return true

	case json.Number:
		// in case the decoder used UseNumber() (which is better in some ways)
		v, err := x.Float64()
		if err != nil {
			// making this up... number parse error is false
			return false
		}

		return !(math.IsNaN(v) || v == 0)

	case float64:
		// in case the decoder did not use UseNumber()
		return !(math.IsNaN(x) || x == 0)

	case int64:
		// in case it's a literal value
		return x != 0

	case nil:
		// null is always false
		return false

	default:
		log.Fatalf("don't know if it's truish: %#v", x)
		panic("todo")
	}
}

//============================================================

type JSONExpr interface {
	String() string
	eval(*jsonContext) (interface{}, error)
}

type BoolAnd struct {
	left, right JSONExpr
}

func (and *BoolAnd) clone(l, r JSONExpr) JSONExpr {
	return &BoolAnd{
		left:  l,
		right: r,
	}
}

func (and *BoolAnd) String() string {
	return "(" + and.left.String() + " && " + and.right.String() + ")"
}

func (and *BoolAnd) eval(j *jsonContext) (interface{}, error) {
	l, err := and.left.eval(j)
	if err != nil {
		return nil, err
	}
	if !truish(l) {
		return l, nil
	}
	return and.right.eval(j)
}

type BoolOr struct {
	left, right JSONExpr
}

func (or *BoolOr) clone(l, r JSONExpr) JSONExpr {
	return &BoolOr{
		left:  l,
		right: r,
	}
}

func (or *BoolOr) String() string {
	return "(" + or.left.String() + " || " + or.right.String() + ")"
}

func (or *BoolOr) eval(j *jsonContext) (interface{}, error) {
	l, err := or.left.eval(j)
	if err != nil {
		return nil, err
	}
	if truish(l) {
		return l, nil
	}
	return or.right.eval(j)
}

type DocRef struct {
}

func (dr *DocRef) String() string {
	return "@"
}

func (dr *DocRef) eval(j *jsonContext) (interface{}, error) {
	return j.doc, nil
}

type FieldDeref struct {
	lhs   JSONExpr
	field string
}

func (fd *FieldDeref) String() string {
	return fd.lhs.String() + "." + fd.field
}

func (fd *FieldDeref) eval(j *jsonContext) (interface{}, error) {
	lvalue, err := fd.lhs.eval(j)
	if err != nil {
		return nil, err
	}
	if obj, ok := lvalue.(map[string]interface{}); ok {
		if item, ok := obj[fd.field]; ok {
			log.Info("Field deref => %#v", item)
			return item, nil
		} else {
			log.Error("%#v has no such property %q", lvalue, fd.field)
			return nil, ErrNoSuchProperty
		}
	} else if str, ok := lvalue.(string); ok {
		if fd.field == "length" {
			return int64(len(str)), nil
		} else {
			return nil, ErrNoSuchProperty
		}
	} else {
		log.Error("Not an object: %#v", lvalue)
		return nil, ErrNotAnObject
	}
}

type ArrayRef struct {
	lhs JSONExpr
	arg JSONExpr
}

func (aref *ArrayRef) String() string {
	return aref.lhs.String() + "[" + aref.arg.String() + "]"
}

func (aref *ArrayRef) eval(j *jsonContext) (interface{}, error) {
	lhs, err := aref.lhs.eval(j)
	if err != nil {
		return nil, err
	}
	index, err := aref.arg.eval(j)
	if err != nil {
		return nil, err
	}

	if array, ok := lhs.([]interface{}); ok {
		index, err := intish(index)
		if err != nil {
			return nil, err
		}
		if index >= 0 && index < int64(len(array)) {
			return array[index], nil
		}
		return nil, ErrArrayIndexOutOfRange
	} else if dict, ok := lhs.(map[string]interface{}); ok {
		str, err := stringish(index)
		if err != nil {
			return nil, err
		}
		if value, ok := dict[str]; ok {
			return value, nil
		}
		return nil, ErrNoSuchProperty
	} else {
		// TODO should we support subscripting strings?
		return nil, ErrNotSubscriptable
	}
}

type MethodCall struct {
	lhs    JSONExpr
	method string
	args   []JSONExpr
}

func (mc *MethodCall) String() string {
	buf := &bytes.Buffer{}
	buf.WriteString(mc.lhs.String())
	buf.WriteByte('.')
	buf.WriteString(mc.method)
	buf.WriteByte('(')
	for i, arg := range mc.args {
		if i > 0 {
			buf.WriteByte(',')
		}
		buf.WriteString(arg.String())
	}
	buf.WriteByte(')')
	return buf.String()
}

func (mc *MethodCall) eval(j *jsonContext) (interface{}, error) {
	// !! no methods are supported(yet)
	return nil, ErrMethodNotSupported
}

type Literal struct {
	value interface{}
}

func (lit *Literal) String() string {
	buf, _ := json.Marshal(lit.value)
	return string(buf)
}

func (lit *Literal) eval(j *jsonContext) (interface{}, error) {
	return lit.value, nil
}

type Compare struct {
	left, right JSONExpr
	op          earley.OpCode
}

func (cmp *Compare) String() string {
	return cmp.left.String() + " " + cmpOpStr[cmp.op] + " " + cmp.right.String()
}

func (cmp *Compare) eval(j *jsonContext) (interface{}, error) {
	left, err := cmp.left.eval(j)
	if err != nil {
		return nil, err
	}

	right, err := cmp.right.eval(j)
	if err != nil {
		return nil, err
	}

	lt, eq, gt, err := compare(left, right)
	if err != nil {
		return nil, err
	}
	switch cmp.op {
	case CmpLT:
		return lt, nil
	case CmpGT:
		return gt, nil
	case CmpLE:
		return lt || eq, nil
	case CmpGE:
		return gt || eq, nil
	case CmpEQ:
		return eq, nil
	case CmpNE:
		return !eq, nil
	}
	panic("invalid operator in Compare node")
}

// compile-time check to make sure everything we expect is a JSONExpr...
var samples = []JSONExpr{
	&BoolAnd{},
	&BoolOr{},
	&DocRef{},
	&FieldDeref{},
	&ArrayRef{},
	&Literal{},
	&MethodCall{},
	&Compare{},
}
