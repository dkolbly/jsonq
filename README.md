JSON Querying
=============

This package provides reasonable implementations of JSON
querying facilities, notably JSON Path and JSON Predicates.

JSON Path
---------
Conceptually, you can think of a JSON Path expression as a predicate
over paths through a JSON document.  The result of a JSON Path
evaluation is all the paths which match the predicate.

For example, consider the document:

    {
      "first": "John",
      "last": "Doe",
      "phones": [
        {
          "type": "home",
          "number": "+1 123-456-7889"
        },
        {
          "type": "cell",
          "number": "+1 456-789-1234"
        }
      ]
    }

A path through a document is a (possibly empty) sequence of
either property names or non-negative array indices.  That
document has the following concrete paths:

| Path                     | Value                          | 
| -----------------------  | ------------------------------ | 
| []                       | {"first":"John", ...}          | 
| ["first"]                | "John"                         | 
| ["last"]                 | "Doe"                          | 
| ["phones"]               | [{"type":"home",...}...]       | 
| ["phones", 0]            | {"type":"home","number", ...}  | 
| ["phones", 0, "type"]    | "home"                         | 
| ["phones", 0, "number"]  | "+1 123-456-7889"              | 
| ["phones", 1]            | {"type":"cell","number", ...}  | 
| ["phones", 1, "type"]    | "cell"                         | 
| ["phones", 1, "number"]  | "+1 456-789-1234"              | 
                                                              
                                                              
Consider a JSON path expression `$.phones[1]`; you can ask    
the question: _which of the paths does this expression match?_
                                                              
| Path                    | Answer                            | 
| ----------------------- | ------------------------------    | 
| []                      | No, path is too short             | 
| ["first"]               | No, path is too short             | 
| ["last"]                | No, path is too short             | 
| ["phones"]              | No, path is too short             | 
| ["phones", 0]           | No, second element of path != 1   | 
| ["phones", 0, "type"]   | No, path is too long              | 
| ["phones", 0, "number"] | No, path is too long              | 
| ["phones", 1]           | Yes                               | 
| ["phones", 1, "type"]   | No, path is too long              | 
| ["phones", 1, "number"] | No, path is too long              | 
                                                              
Hence, evaluating `$.phones[1]` against the sample document   
produces exactly one path `["phones", 1]`.  That looks easy   
because it's almost just a different representation of the path.
                                                              
However, JSON path can be more expressive.  Consider the      
JSON path expression `$.phones[?(@.type == "cell")]`.         
                                                              
| Path                    | Answer                                             | 
| ----------------------- | -------------------------------------------------- | 
| []                      | No, path is too short                              | 
| ["first"]               | No, path is too short                              | 
| ["last"]                | No, path is too short                              | 
| ["phones"]              | No, path is too short                              | 
| ["phones", 0]           | No, the object does not satisfy @.type == "cell"   | 
| ["phones", 0, "type"]   | No, path is too long                               | 
| ["phones", 0, "number"] | No, path is too long                               | 
| ["phones", 1]           | Yes                                                | 
| ["phones", 1, "type"]   | No, path is too long                               | 
| ["phones", 1, "number"] | No, path is too long                               | 
                                                                               
Of course, this is not a terribly efficient way to evaluate
JSON path expressions, but it helps to visualize the
semantics, which can be useful when debugging expressions.

### Quirks

Some things turn out to be hard to express.  Writing a JSON Path
expression to match a specific value in a document is tricky.  Let's
say we want to produce a match in the above document if the value of
the `first` property is `"John"`.  You'd might try to write
`$[?(@.first == "John")]` but that doesn't work because `[...]` says
to dereference the properties of the node so by the time you get to
run the test, `@` is already the sub-object.  No problem, you say!
You can fix that with `$[?(@ == "John")]` Which does indeed, sort of,
work.

| Path                    | Answer
| ----------------------- | ------------------------------
| []                      | No, path is too short
| ["first"]               | Yes, @ == "John"
| ["last"]                | No, @ == "Doe"
| ["phones"]              | No, @ == `{...}`
| ["phones", 0]           | No, path is too long
| ["phones", 0, "type"]   | No, path is too long
| ["phones", 0, "number"] | No, path is too long
| ["phones", 1]           | No, path is too long
| ["phones", 1, "type"]   | No, path is too long
| ["phones", 1, "number"] | No, path is too long
                          
The problem is that this will match if *any* property at that
level has the value `"John"`, not just the `first` property.

You might also try to fix that by doing `$?(@.first == "John")` but at
least the reference Javascript implementation doesn't support that,
although it's not clear from the (very short) spec if that's expected
or not.

For my purposes, I regard it as a bug in the implementation --
`?(...)` should be able to apply at any point in the JSON path filter
essentially as a post-condition, so that for me `[?(...)]` is
equivalent to `[*]?(...)`.  I believe the in reference implementation,
`[?(...)]` is treated as a special case.


