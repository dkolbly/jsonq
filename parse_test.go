package jsonq

import (
	"testing"
)

type pathExprParseCase struct {
	given  string
	expect string
	fail   bool
}

var pathParseCases = []pathExprParseCase{
	{
		given:  `$`,
		expect: `$`,
	},
	{
		given:  `$ . foo`,
		expect: `$.foo`,
	},
	{
		given: `foo`,
		fail:  true,
	},
	{
		given: `$.foo$`,
		fail:  true,
	},
	{
		given:  `.foo .x`,
		expect: `.foo.x`,
	},
	{
		given:  ".foo .x\n.y",
		expect: `.foo.x.y`,
	},
	// 001
	{
		given:  ".foo [::2]",
		expect: `.foo[::2]`,
	},
	// 010
	{
		given:  ".foo [:-1]",
		expect: `.foo[:-1]`,
	},
	// 011
	{
		given:  ".foo [:1:2]",
		expect: `.foo[:1:2]`,
	},
	// 100
	{
		given:  ".foo [3]",
		expect: `.foo[3]`,
	},
	// 101
	{
		given:  ".foo [1::2]",
		expect: `.foo[1::2]`,
	},
	// 110
	{
		given:  ".foo [1:-1]",
		expect: `.foo[1:-1]`,
	},
	// 111
	{
		given:  ".foo [1:2:3]",
		expect: `.foo[1:2:3]`,
	},
	//[13]
	{
		given:  `$. . foo`,
		expect: `$..foo`,
	},
	//
	{
		given:  ".foo [3:]",
		expect: `.foo[3:]`,
	},
	//
	{
		given:  `$ . . *`,
		expect: `$..[*]`,
	},
	//
	{
		given:  `$ . phones [?(@)]`,
		expect: `$.phones[?(@)]`,
	},
	//
	{
		given:  `$ . phones[ ?( @.foo ) ]`,
		expect: `$.phones[?(@.foo)]`,
	},
	//
	{
		given:  `$..`,
		expect: `$..`,
	},
	//
	{
		given:  `$.phones[2][*]`,
		expect: `$.phones[2][*]`,
	},
	//
	{
		given:  `$.phones..[?(@.is_cell)]`,
		expect: `$.phones..[?(@.is_cell)]`,
	},
	//
	{
		given:  `$..[?(@.phones[0])]`, // reduceArrayRef
		expect: `$..[?(@.phones[0])]`,
	},
	//
	{
		given:  `$..[?(@. length () )]`, // reduceMethodCall, reduceEmptyArgList
		expect: `$..[?(@.length())]`,
	},
	//
	{
		given:  `$..[?(@. foo ( "x" ) )]`,
		expect: `$..[?(@.foo("x"))]`,
	},
	//
	{
		given:  `$..[?(@. foo ( "x", "y" ) )]`,
		expect: `$..[?(@.foo("x","y"))]`,
	},
	//
	{
		given:  `$..[?(@.foo == "a\x0az" )]`,
		expect: `$..[?(@.foo == "a\nz")]`,
	},
	//
	{
		given:  `$..[?(@.foo == "a\012z" )]`,
		expect: `$..[?(@.foo == "a\nz")]`,
	},
	//
	{
		given:  `$..[?(@.foo == "a\u000az" )]`,
		expect: `$..[?(@.foo == "a\nz")]`,
	},
	//
	{
		given:  `$..[?(@.foo == "a\U0000000az" )]`,
		expect: `$..[?(@.foo == "a\nz")]`,
	},
	//
	{
		given:  `$..[?(@.foo  >  3)].x`,
		expect: `$..[?(@.foo > 3)].x`,
	},
	//
	{
		given:  `$..[?(@.foo  <  3)].x`,
		expect: `$..[?(@.foo < 3)].x`,
	},
	//
	{
		given:  `$..[?(@.foo  >=  3)].x`,
		expect: `$..[?(@.foo >= 3)].x`,
	},
	//
	{
		given:  `$..[?(@.foo  <=  3)].x`,
		expect: `$..[?(@.foo <= 3)].x`,
	},
	//
	{
		given:  `$..[?(@.foo  ==  3)].x`,
		expect: `$..[?(@.foo == 3)].x`,
	},
	//
	{
		given:  `$..[?(@.foo  !=  3)].x`,
		expect: `$..[?(@.foo != 3)].x`,
	},
}

func TestPathParse(t *testing.T) {
	for i, c := range pathParseCases {
		log.Info("PathParse Case[%d] %q", i, c.given)
		expr, err := PathParse(c.given)
		if c.fail {
			if err == nil {
				t.Errorf("Case[%d] expected to fail, but didn't", i)
			}
		} else {
			if err != nil {
				t.Errorf("Case[%d] expected to succeed, but failed with: %s",
					i,
					err)
			} else if c.expect != expr.String() {
				t.Errorf("Case[%d] expected to produce %q, but parsed as %q",
					i,
					c.expect,
					expr.String())
			}
		}
	}
}
