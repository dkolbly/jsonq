package jsonq

import (
	"bitbucket.org/dkolbly/earley"
)

var pathScanLang = makePathScanLang()

const (
	FilterOp = earley.OpCode(iota + 100)
	DocRefOp
	BoolAndOp
	BoolOrOp
	TrueOp
	FalseOp
	CmpLT
	CmpLE
	CmpGT
	CmpGE
	CmpEQ
	CmpNE
)

var cmpOpStr = map[earley.OpCode]string{
	CmpLT: "<",
	CmpLE: "<=",
	CmpGT: ">",
	CmpGE: ">=",
	CmpEQ: "==",
	CmpNE: "!=",
}

func makePathScanLang() *earley.RegexLang {
	re := earley.NewRegexLang()
	re.AddSpace(`\s`, `\s+`)
	re.AddOp(`:`, `:`, earley.Colon)
	re.AddOp(`\*`, `\*`, earley.Times)
	re.AddOp(`\[`, `\[`, earley.OpenBracket)
	re.AddOp(`\]`, `\]`, earley.CloseBracket)
	re.AddOp(`\$`, `\$`, earley.Dollar)
	re.AddOp(`\.`, `\.`, earley.Dot)
	re.AddBuiltin(`[a-zA-Z_]`, `[a-zA-Z_]\w*`, &earley.Identifier{})
	re.AddBuiltin(`[-0-9]`, `[-0-9][0-9]*`, &earley.Integer{})
	re.AddOp(`\(`, `\(`, earley.OpenParen)
	re.AddOp(`\?`, `\?\(`, FilterOp)
	re.AddOp(`\)`, `\)`, earley.CloseParen)
	re.AddOp(`@`, `@`, DocRefOp)
	re.AddOp(`&`, `&&`, BoolAndOp)
	re.AddOp(`\|`, `\|\|`, BoolOrOp)
	re.AddOp(`,`, `,`, earley.Comma)
	re.AddOp(`t`, `true`, TrueOp)
	re.AddOp(`f`, `false`, FalseOp)
	re.AddBuiltinString(`"`, 0)
	re.AddBuiltinString(`'`, 0)

	re.AddOp(`<`, `<`, CmpLT)
	re.AddOp(`<`, `<=`, CmpLE)
	re.AddOp(`>`, `>`, CmpGT)
	re.AddOp(`>`, `>=`, CmpGE)
	re.AddOp(`=`, `==`, CmpEQ)
	re.AddOp(`!`, `!=`, CmpNE)

	return re
}
