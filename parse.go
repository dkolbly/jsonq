package jsonq

import (
	"bytes"
	"context"
	"fmt"
	"strconv"

	"bitbucket.org/dkolbly/earley"
)

func PathParse(expr string) (*Expr, error) {
	// TODO cache this
	g := PathGrammar()
	m, err := g.Parse(context.Background(),
		bytes.NewReader([]byte(expr)),
		nil)
	if err != nil {
		return nil, err
	}
	return m[0].(*Expr), nil
}

func PathGrammar() *earley.Lang {

	defer func() {
		panick := recover()
		if panick != nil {
			panic(fmt.Sprintf("Not able to build PathGrammar!\n%s", panick))
		}
	}()

	start := earley.Intern("start")
	expr := earley.Intern("expr")
	filter := earley.Intern("filter")
	filterseq := earley.Intern("filter*")

	qual := earley.Intern("qual")
	boolq := earley.Intern("boolq")
	slice := earley.Intern("slice")

	term := earley.Intern("term")
	factor := earley.Intern("factor")
	field := earley.Intern("field")
	primitive := earley.Intern("primitive")
	arglist := earley.Intern("arglist")
	arglistne := earley.Intern("arglist+")

	lang := earley.NewLang(start, pathScanLang)
	var INT = intMatcher{}
	var IDENT = identMatcher{}
	var STRING = stringMatcher{}
	var CMPOP = cmpOpMatcher{}

	// start -> expr
	lang.Add(lang.Rule(start, passthru, expr))

	// expr -> "$" filterseq
	lang.Add(lang.Rule(expr, anchoredPath, "$", filterseq))

	// expr -> filterseq
	lang.Add(lang.Rule(expr, unanchoredPath, filterseq))

	// filter* ->
	lang.Add(lang.Rule(filterseq, reduceFinalMatch))

	// filter* -> filter filter*
	lang.Add(lang.Rule(filterseq, moreFilters, filter, filterseq))

	// filter -> "." IDENT
	lang.Add(lang.Rule(filter, reducePropertyFilter, ".", IDENT))

	// filter -> "."
	// (Note that this can allow syntactically some invalid things,
	// like $...foo, but that usually works out fine or is checked
	// in the cons function)
	lang.Add(lang.Rule(filter, reduceRecursiveScanFilter, "."))

	// these mean the same thing
	lang.Add(lang.Rule(filter, reduceWildcardFilter, ".", "*"))
	lang.Add(lang.Rule(filter, reduceWildcardFilter, "[", "*", "]"))

	// filter -> "[" slice "]"
	lang.Add(lang.Rule(filter, reduceSlicer, "[", slice, "]"))

	// filter -> "[" "?(" qual ")" "]"
	lang.Add(lang.Rule(filter, reduceQualifiedScan, "[", "?(", qual, ")", "]"))

	// filter -> qual
	lang.Add(lang.Rule(filter, reduceQualifier, "?(", qual, ")"))

	//==================== qual ====================
	// qualifier expressions

	// qual -> boolq
	lang.Add(lang.Rule(qual, passthru, boolq))

	// qual -> boolq "&&" qual
	lang.Add(lang.Rule(qual, binaryReducer(&BoolAnd{}), boolq, "&&", qual))

	// qual -> boolq "||" qual
	lang.Add(lang.Rule(qual, binaryReducer(&BoolOr{}), boolq, "||", qual))

	// boolq -> term
	lang.Add(lang.Rule(boolq, passthru, term))

	// boolq -> term CMPOP term
	lang.Add(lang.Rule(boolq, reduceCmp, term, CMPOP, term))

	// term -> factor
	lang.Add(lang.Rule(term, passthru, factor))

	// factor -> field
	lang.Add(lang.Rule(factor, passthru, field))

	// field -> primitive
	lang.Add(lang.Rule(field, passthru, primitive))

	// field -> field "." IDENT
	lang.Add(lang.Rule(field, reduceFieldDeref, field, ".", IDENT))

	// field -> field "[" term "]"
	lang.Add(lang.Rule(field, reduceArrayRef, field, "[", term, "]"))

	// field -> field "." IDENT "(" arglist ")"
	lang.Add(lang.Rule(field, reduceMethodCall, field, ".", IDENT, "(", arglist, ")"))

	// arglist ->
	lang.Add(lang.Rule(arglist, reduceEmptyArgList))

	// arglist -> arglistne
	lang.Add(lang.Rule(arglist, passthru, arglistne))

	// arglistne -> term
	lang.Add(lang.Rule(arglistne, reduceSingleArg, term))

	// arglistne -> arglistne "," term
	lang.Add(lang.Rule(arglistne, reduceExtendArg, arglistne, ",", term))

	// primitive -> "(" qual ")"
	lang.Add(lang.Rule(primitive, passthru1, "(", qual, ")"))

	// primitive -> "true"
	lang.Add(lang.Rule(primitive, reduceTrueLiteral, "true"))

	// primitive -> "false"
	lang.Add(lang.Rule(primitive, reduceFalseLiteral, "false"))

	// primitive -> INT
	lang.Add(lang.Rule(primitive, reduceIntLiteral, INT))

	// primitive -> STRING
	lang.Add(lang.Rule(primitive, reduceStringLiteral, STRING))

	// primitive -> "@"
	lang.Add(lang.Rule(primitive, reduceDocRef, "@"))

	//==================== slices ====================

	// slice -> INT ":" INT ":" INT
	lang.Add(lang.Rule(slice, reduceSlice3iii, INT, ":", INT, ":", INT))

	// slice -> INT
	lang.Add(lang.Rule(slice, reduceSlice1i, INT))

	// slice -> INT ":" INT
	lang.Add(lang.Rule(slice, reduceSlice2ii, INT, ":", INT))

	// slice -> ":" INT
	lang.Add(lang.Rule(slice, reduceSlice2xi, ":", INT))

	// slice -> INT ":"
	lang.Add(lang.Rule(slice, reduceSlice2ix, INT, ":"))

	// slice -> ":" ":" INT
	lang.Add(lang.Rule(slice, reduceSlice3xxi, ":", ":", INT))

	// slice -> ":" INT ":" INT
	lang.Add(lang.Rule(slice, reduceSlice3xii, ":", INT, ":", INT))

	// slice -> INT ":" ":" INT
	lang.Add(lang.Rule(slice, reduceSlice3ixi, INT, ":", ":", INT))
	return lang
}

//======================================================================
//                              TOKEN MATCHERS
//======================================================================

type cmpOpMatcher struct {
}

func (cmp cmpOpMatcher) String() string {
	return "<CMP>"
}

func (cmp cmpOpMatcher) Match(t earley.Token) bool {
	op, ok := t.(*earley.Operator)
	if !ok {
		return false
	}
	switch op.Code {
	case CmpLT, CmpLE, CmpGT, CmpGE, CmpEQ, CmpNE:
		return true
	default:
		return false
	}
}

// integers

type intMatcher struct {
}

func (im intMatcher) String() string {
	return "INT"
}

func (im intMatcher) Match(t earley.Token) bool {
	_, ok := t.(*earley.Integer)
	return ok
}

// string

type stringMatcher struct {
}

func (stringMatcher) String() string {
	return "STRING"
}

func (stringMatcher) Match(t earley.Token) bool {
	_, ok := t.(*earley.String)
	return ok
}

// identifiers
type identMatcher struct {
}

func (im identMatcher) String() string {
	return "IDENT"
}

func (im identMatcher) Match(t earley.Token) bool {
	_, ok := t.(*earley.Identifier)
	return ok
}

//======================================================================

type Expr struct {
	Anchored bool
	Filter   Filter
}

func (e *Expr) String() string {
	if e.Anchored {
		return "$" + e.Filter.String()
	}
	return e.Filter.String()
}

type Filter interface {
	String() string
	cons(rest Filter) Filter
	find(ctx *searchctx, doc interface{}, at *pnode)
}

type finalMatch struct {
}

func (f finalMatch) String() string {
	return ""
}

func (f finalMatch) cons(rest Filter) Filter {
	panic("woops, not allowed to cons final match onto anything")
}

type propertyFilter struct {
	propertyName string
	rest         Filter
}

func (pf *propertyFilter) String() string {
	return "." + pf.propertyName + pf.rest.String()
}

func (pf *propertyFilter) cons(rest Filter) Filter {
	return &propertyFilter{
		propertyName: pf.propertyName,
		rest:         rest,
	}
}

func reduceFinalMatch(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	var f Filter
	f = finalMatch{}
	return e, f
}

func reducePropertyFilter(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	id := m[1].(*earley.Identifier).Value
	return e, &propertyFilter{
		propertyName: id,
		rest:         finalMatch{},
	}
}

func moreFilters(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, m[0].(Filter).cons(m[1].(Filter))
}

func passthru(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, m[0]
}

func passthru1(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, m[1]
}

func anchoredPath(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &Expr{
		Anchored: true,
		Filter:   (m[1].(Filter)),
	}
}

func unanchoredPath(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &Expr{
		Filter: (m[0].(Filter)),
	}
}

type sliceFilter struct {
	spec sliceSpec
	rest Filter
}

func (sf *sliceFilter) String() string {
	var ret []byte
	ret = append(ret, '[')
	if sf.spec.flags&startFlag != 0 {
		ret = strconv.AppendInt(ret, sf.spec.start, 10)
	}
	if sf.spec.flags&singleFlag == 0 {
		ret = append(ret, ':')
		if sf.spec.flags&endFlag != 0 {
			ret = strconv.AppendInt(ret, sf.spec.end, 10)
		}

		if sf.spec.flags&stepFlag != 0 {
			ret = append(ret, ':')
			ret = strconv.AppendInt(ret, sf.spec.step, 10)
		}
	}
	ret = append(ret, ']')
	return string(ret) + sf.rest.String()
}

func (sf *sliceFilter) cons(rest Filter) Filter {
	return &sliceFilter{
		spec: sf.spec,
		rest: rest,
	}
}

func reduceSlicer(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	ss := m[1].(*sliceSpec)
	return e, &sliceFilter{spec: *ss}
}

const (
	singleFlag = 1 << iota
	startFlag
	endFlag
	stepFlag
)

type sliceSpec struct {
	flags            uint8
	start, end, step int64
}

func reduceSlice3iii(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &sliceSpec{
		start: m[0].(*earley.Integer).Int64(),
		end:   m[2].(*earley.Integer).Int64(),
		step:  m[4].(*earley.Integer).Int64(),
		flags: startFlag | endFlag | stepFlag,
	}
}

func reduceSlice1i(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &sliceSpec{
		start: m[0].(*earley.Integer).Int64(),
		flags: startFlag | singleFlag,
	}
}

func reduceSlice2ii(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &sliceSpec{
		start: m[0].(*earley.Integer).Int64(),
		end:   m[2].(*earley.Integer).Int64(),
		flags: startFlag | endFlag,
	}
}

func reduceSlice2xi(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &sliceSpec{
		end:   m[1].(*earley.Integer).Int64(),
		flags: endFlag,
	}
}

func reduceSlice2ix(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &sliceSpec{
		start: m[0].(*earley.Integer).Int64(),
		flags: startFlag,
	}
}

func reduceSlice3xxi(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &sliceSpec{
		step:  m[2].(*earley.Integer).Int64(),
		flags: stepFlag,
	}
}

func reduceSlice3ixi(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &sliceSpec{
		start: m[0].(*earley.Integer).Int64(),
		step:  m[3].(*earley.Integer).Int64(),
		flags: startFlag | stepFlag,
	}
}

func reduceSlice3xii(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &sliceSpec{
		end:   m[1].(*earley.Integer).Int64(),
		step:  m[3].(*earley.Integer).Int64(),
		flags: endFlag | stepFlag,
	}
}

//============================================================

type recursiveScanFilter struct {
	rest Filter
}

func (ps *recursiveScanFilter) String() string {
	rest := ps.rest.String()
	if len(rest) > 0 && rest[0] == '.' {
		return "." + rest
	} else {
		return ".." + rest
	}
}

func (ps *recursiveScanFilter) cons(rest Filter) Filter {
	// collapse repeated recursive scan filters
	if _, stacked := rest.(*recursiveScanFilter); stacked {
		return rest
	}
	return &recursiveScanFilter{
		rest: rest,
	}
}

func reduceRecursiveScanFilter(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &recursiveScanFilter{}
}

//============================================================

type wildcardFilter struct {
	rest Filter
}

func (w *wildcardFilter) String() string {
	return "[*]" + w.rest.String()
}

func (w *wildcardFilter) cons(rest Filter) Filter {
	return &wildcardFilter{
		rest: rest,
	}
}

func reduceWildcardFilter(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &wildcardFilter{}
}

//============================================================

type postFilter struct {
	rest Filter
	expr JSONExpr
}

func (post *postFilter) String() string {
	return "?(" + post.expr.String() + ")" + post.rest.String()
}

func (post *postFilter) cons(rest Filter) Filter {
	return &postFilter{
		rest: rest,
		expr: post.expr,
	}
}

func reduceQualifier(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &postFilter{
		expr: m[1].(JSONExpr),
	}
}

//============================================================

type qualifiedScan struct {
	rest Filter
	expr JSONExpr
}

func (qa *qualifiedScan) String() string {
	return "[?(" + qa.expr.String() + ")]" + qa.rest.String()
}

func (qa *qualifiedScan) cons(rest Filter) Filter {
	return &qualifiedScan{
		rest: rest,
		expr: qa.expr,
	}
}

func reduceQualifiedScan(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &qualifiedScan{
		expr: m[2].(JSONExpr),
	}
}

type binaryNode interface {
	clone(a, b JSONExpr) JSONExpr
}

func binaryReducer(example binaryNode) func(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return func(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
		left := m[0].(JSONExpr)
		right := m[2].(JSONExpr)
		return e, example.clone(left, right)
	}
}

func reduceDocRef(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &DocRef{}
}

func reduceFieldDeref(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &FieldDeref{
		lhs:   m[0].(JSONExpr),
		field: m[2].(*earley.Identifier).Value,
	}
}

func reduceArrayRef(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &ArrayRef{
		lhs: m[0].(JSONExpr),
		arg: m[2].(JSONExpr),
	}
}

func reduceMethodCall(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &MethodCall{
		lhs:    m[0].(JSONExpr),
		method: m[2].(*earley.Identifier).Value,
		args:   m[4].([]JSONExpr),
	}
}

func reduceEmptyArgList(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, []JSONExpr{}
}

func reduceSingleArg(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	expr := m[0].(JSONExpr)
	return e, []JSONExpr{expr}
}

func reduceExtendArg(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	arglist := m[0].([]JSONExpr)
	expr := m[2].(JSONExpr)

	return e, append(arglist, expr)
}

func reduceIntLiteral(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &Literal{
		value: m[0].(*earley.Integer).Int64(),
	}
}

func reduceStringLiteral(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &Literal{
		value: m[0].(*earley.String).Value,
	}
}

func reduceTrueLiteral(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &Literal{
		value: true,
	}
}

func reduceFalseLiteral(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &Literal{
		value: false,
	}
}

func reduceCmp(e earley.Envt, m []earley.Meaning) (earley.Envt, earley.Meaning) {
	return e, &Compare{
		left:  m[0].(JSONExpr),
		right: m[2].(JSONExpr),
		op:    m[1].(*earley.Operator).Code,
	}
}
